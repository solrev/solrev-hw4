import pygame
import random
 
black = (0, 0, 0)
white = (255, 255, 255)
blue =  (0, 0, 255)
red = (255, 0, 0)
green = (0, 255, 0)
 
 
# Define CLASSES below:
 
# Player class is for the paddles on each side
# Player class is derived from sprite class in Pygame
class Player(pygame.sprite.Sprite):

    # Player shape
    width = 10
    height = 75
    
    # Set speed vector (Player only moves in y-direction)
    change_y = 0
    walls = None 

     
    # Constructor. Pass in the color of the block, and its x and y position
    def __init__(self, x, y):
        # Call the parent class (Sprite) constructor
        super(Player, self).__init__() 
 
        # Variables to hold the height and width of the block
 
        # Create an image of the ball, and fill it with a color.
        # This could also be an image loaded from the disk.
        self.image = pygame.Surface([self.width, self.height])
        self.image.fill(white)
 
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()
         
        # Set initial position of sprite to 100,100
        self.rect.x = x
        self.rect.y = y
         
    def changespeed(self, y):
        """ Change the speed of the player. """
        self.change_y += y        
         
 
    def update(self):
        """ Update the player's position. """
        # Move up/down
        self.rect.y += self.change_y
        
        # If the user moves past the top/bottom of the screen, set the position
        # to the edge.
        if self.rect.y < 0:
                self.rect.y = 0
        if self.rect.y > screen_height - self.height:
                self.rect.y = screen_height - self.height
        
 
class Wall(pygame.sprite.Sprite):
    """ This class represents the wall at the top and bottom of the
        screen. """
 
    # Constructor function
    def __init__(self, x, y, width, height):
        # Call the parent's constructor
        super(Wall, self).__init__()
 
        # Make a blue wall, of the size specified in the parameters
        self.image = pygame.Surface([width, height])
        self.image.fill((blue))
 
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x
         
class Ball(pygame.sprite.Sprite):
    """ This class represents the ball that bounces around. """
 
    # Set speed vector
    change_x = 0
    change_y = 0
    walls = None
     
    # Constructor function
    def __init__(self, x, y, walls, players):
        # Call the parent's constructor
        super(Ball, self).__init__()
  
        # Set height, width
        self.image = pygame.Surface([15, 15])
        self.image.fill(white)
 
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x
         
        self.walls = walls
        self.players = players
        
        self.hit_count = 0
         
    def update(self):
        """ Updat the ball's position. """
        # Get the old position, in case we need to go back to it
        old_x = self.rect.x
        new_x = old_x + self.change_x
        self.rect.x = new_x
         
        # Did this update cause us to hit a wall?
        collide = pygame.sprite.spritecollide(self, self.walls, False)
        if collide:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1
 
        old_y = self.rect.y
        new_y = old_y + self.change_y
        self.rect.y = new_y
         
        # Did this update cause us to hit a wall?
        collide = pygame.sprite.spritecollide(self, self.walls, False)
        if collide:
            # Whoops, hit a wall. Go back to the old position
            self.rect.y = old_y
            self.change_y *= -1
             
        if self.rect.x < -20 or self.rect.x > screen_width + 20:
            self.change_x = 0
            self.change_y = 0
        """    
        hit = pygame.sprite.spritecollide(self, self.players, False)
        if hit:
            self.hit_count += 1
            
        if hit > 3:
            self.change_x +=10
            self.change_y +=10
        """
             
# Call this function so the Pygame library can initialize itself
pygame.init()
 
# Create an 800x600 sized screen
screen_width = 800
screen_height = 600
 
screen = pygame.display.set_mode([screen_width, screen_height])
 
# Set the title of the window
pygame.display.set_caption('PONG')
 
# Create a surface we can draw on
background = pygame.Surface(screen.get_size())
 
# Used for converting color maps and such
background = background.convert()
 
# Fill the screen with a black background
background.fill(black)
 
# All sprite lists
wall_list = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
movingsprites = pygame.sprite.Group()
player_list = pygame.sprite.Group() # for player 1 and 2 to keep track of hits

# Hit count list
#hit_count = 0

# Create the players
player1 = Player(10, screen_height / 2)
player_list.add(player1) # add to list of players for ball collision detection
all_sprites.add(player1)
wall_list.add(player1)
movingsprites.add(player1)
 
player2 = Player(screen_width - 20, screen_height / 2)
player_list.add(player2) # add to list of players for ball collision detection
all_sprites.add(player2)
wall_list.add(player2)
movingsprites.add(player2)
 
# Make the walls. (x_pos, y_pos, width, height)
# Top wall
wall = Wall(0, 0, screen_width, 10) 
wall_list.add(wall)
all_sprites.add(wall)
 
# Bottom wall
wall = Wall(0, screen_height - 10, screen_width, screen_height) 
wall_list.add(wall)
all_sprites.add(wall)
 
# Create the ball
ball = Ball( -50, -50, wall_list, player_list )
movingsprites.add(ball)
all_sprites.add(ball)
 
clock = pygame.time.Clock()
 
done = False
 
# Main program loop
while not done:
     
    # Loop through any window events
    for event in pygame.event.get():
        # The user clicked 'close' or hit Alt-F4
        if event.type == pygame.QUIT:
            done = True
        # The user clicked the mouse button
        # or pressed a key
        
        
        # Is the ball not moving?
        if ball.change_y == 0:
             
            # Start in the middle of the screen at a random y location
            ball.rect.x = screen_width/2
            ball.rect.y = random.randrange(10, screen_height - 10)
             
            # Set a random vector
            ball.change_y = random.randrange(-5, 6)
            ball.change_x =  random.randrange(5, 10)
             
            # Is the ball headed left or right? Select randomly
            if( random.randrange(2) == 0 ):
                ball.change_x *= -1
        
        if event.type == pygame.KEYDOWN:
            # Control player2 movement with up/down keys for KEYDOWN event
            if event.key == pygame.K_UP:
                player2.changespeed(-10) # because pixels are going down screen
            elif event.key == pygame.K_DOWN:
                player2.changespeed(10)
            # Control player 1 movement with w/s keys for KEYDOWN event
            if event.key == pygame.K_w:
                player1.changespeed(-10)
            elif event.key == pygame.K_s:
                player1.changespeed(10)
                
        elif event.type == pygame.KEYUP:
            # Control player 2 movement with w/s keys for KEYUP event
            if event.key == pygame.K_UP:
                player2.changespeed(10)
            elif event.key == pygame.K_DOWN:
                player2.changespeed(-10)
                
            # Control player 1 movement with w/s keys for KEYUP event
            if event.key == pygame.K_w:
                player1.changespeed(10)
            elif event.key == pygame.K_s:
                player1.changespeed(-10)
                
        
        
                 
    # Update the ball position. Pass it the list of stuff it can bounce off of
    movingsprites.update()
    """
    # need to keep track of number of hits to change ball velocity
    if pygame.sprite.collide_rect(player1, ball):
        hit_count += 1
    if pygame.sprite.collide_rect(player2, ball):
        hit_count += 1
    if hit_count > 4:
        ball.change_x += 15
        ball.change_y += 15
    """
    # Clear the screen
    screen.fill(black)
     
    # Draw the sprites
    all_sprites.draw(screen)
 
    # Display the screen
    pygame.display.flip()
 
    clock.tick(30)
 
# All done, shut down Pygame            
pygame.quit()