# -*- coding: utf-8 -*-
"""
Created on Tue Nov  4 02:46:43 2014

@author: gregkline
"""
import numpy as np
import Tkinter as tk
from PIL import Image, ImageTk

class Game_window(object):
    def __init__(self):
        self.root = tk.Tk()
        self.frame = tk.Frame(self.root, width = 620, height = 480) 
        self.frame.pack()
        self.canvas = tk.Canvas(self.frame, width = 620, height = 480)
        self.canvas.place(x=-2,y=-2)
     
    def Update_window(self,frame):
        self.fr = Image.fromarray(frame,'RGB')  
        self.ph = ImageTk.PhotoImage(image = self.fr)
        self.canvas.create_image(0,0,image=self.ph)
        self.root.update()
        
frame = np.zeros([480,620,3])
for l in range(frame.shape[0]):
    for w in range (frame.shape[1]):
        frame[l,w] = [255,0,0]

img = Image.fromarray(frame,'RGB')

#print frame

gw = Game_window()

gw.Update_window(frame)