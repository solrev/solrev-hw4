# -*- coding: utf-8 -*-
"""
Created on Fri Oct 31 11:08:57 2014

@author: gregkline
"""

import numpy as np
import cmath as cm

a = np.zeros([18,18])

for i in range (2):
    a[:,i] = -1
    a[i,:] = -1

for j in range (2):
    a[:,-1-j] = -1
    a[-1-j,:] = -1

x = 3
y = 3
r = 3
bx = 7
by = 7
c = np.zeros([5,5])
c[x,y]=-2
for i in range(r):
    for j in range(r):
        if cm.sqrt(i**2+j**2).real <= np.floor(r/2):
            c[x+i-1,y+j-1] = -2
            c[x-i-1,y-j-1] = -2
            c[x-i-1,y+j-1] = -2
            c[x+i-1,y-j-1] = -2

a[bx-r:bx+r-1,by-r:by+r-1] = c            
#c = c + np.fliplr(c)
#c = c + np.flipud(c)

print a
print c

#for event, response in zip(self.events, self.responses):
#    if event():
#        response()
#        
#reduce( lambda x,y: x+y )