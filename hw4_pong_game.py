"""
 Show how to bounce a ball with a paddle in Pygame.
 This program assumes there are two joysticks plugged in.
  
 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/
 the above version was the most basic pong, without any scoring or speed
 Modifications were made by
 Dave Westwood and Greg Kline
 to match assignment requirements of Python class

import pygame
import random
import os 

define colors:
 
# Define CLASSES below:
# Player class is for the paddles on each side
# Player class is derived from sprite class in Pygame
class Player(pygame.sprite.Sprite):
    # Player shape
  
    # Set speed vector (Player only moves in y-direction)
    change_y = 0
  
    # Constructor. Pass in the color of the block, and its x and y position
    def __init__(self, x, y,width = 10, height = 75):
        # Call the parent class (Sprite) constructor
        super(Player, self).__init__()  
        # Variables to hold the height and width of the block
        # Create an image of the ball, and fill it with a color.
        # This could also be an image loaded from the disk.
        self.image = pygame.Surface([self.width, self.height])
        self.image.fill(white)
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()        
        # Set initial position of sprite to 100,100
        self.rect.x = x
        self.rect.y = y
        self.x = x
        self.y = y
        self.speed = 10
        self.score = 0
        
    def changespeed(self, y):
         Change the speed of the player. 
        self.change_y += y        
         
    def make_color(self, color):
        self.image.fill(color)

    def update(self):
        Update the player's position. 
        # Move up/down
        self.rect.y += self.change_y
        self.y += self.change_y
        # If the user moves past the top/bottom of the screen, set the position
        # to the edge.
        if self.rect.y < 0:
                self.rect.y = 0
                self.y = 0
        if self.rect.y > screen_height - self.height:
                self.rect.y = screen_height - self.height
                self.y = screen_height - self.height
        
    def change_size(self, width, height): 
        self.image = pygame.Surface([width, height])
        self.image.fill(white)
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()
         # Set initial position of sprite to 100,100
        self.rect.x = self.x
        self.rect.y = self.y

    def change_direction(self):
        #cause the direction to reverse
        self.speed *= -1

    def reset(self, x ,y):
        self.width = 10
        self.height = 75         
        self.image = pygame.Surface([self.width, self.height])
        self.image.fill(white)
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()        
        # Set initial position of sprite to 100,100
        self.rect.x = x
        self.rect.y = y      
        self.speed = 10

class Monster2(pygame.sprite.Sprite):
     This class represents the monster that bounces around. 
    # Set speed vector
    change_x = 0
    change_y = 0
    walls = None
    barriers = None
    
    # Constructor function
    def __init__(self, walls, balls):
        # Call the parent's constructor
        super(Monster2, self).__init__()
        # Set height, width
        self.image = pygame.image.load(os.path.join('images','err.gif'))
#        self.image = pygame.Surface([75, 90])
#        self.image.fill(silver)
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.y = screen_height/2
        self.rect.x = 200       
        # pass is objects monster will react with
        self.walls = walls
        self.balls = balls       
        self.change_x = 0
        self.change_y = 8        
        self.hit_count = 4       
        self.loop = 0
        
    def update(self):
        # Get the old position, in case we need to go back to it
        old_x = self.rect.x
        new_x = old_x + self.change_x
        self.rect.x = new_x        
        old_y = self.rect.y
        new_y = old_y + self.change_y
        self.rect.y = new_y
               
        # when monster collides with wall, reverse direction
        collide_wall = pygame.sprite.spritecollide(self, self.walls, False)
        if collide_wall:
            self.rect.y = old_y
            self.change_y *= -1
            self.loop += 1             
        if (self.loop % 2) == 1:
            if self.change_y < 0 and self.rect.y > screen_height/2:
                self.change_y = 0
                if self.rect.x > screen_width/2:
                    self.change_x = -5
                else:
                    self.change_x = 5
        # trying to get monster to move left and right            
        if self.change_y == 0 and self.rect.x > screen_width/2 + 150:
            self.change_x = 0
            self.change_y = -5
#        elif self.change_y == 0 and self.rect.x < screen_width/2 - 150:
#            self.change_x = 0
#            self.change_y = -5
            
    def red_hits(self):
        self.hit_count -= 1        
        if self.hit_count == 0:
            self.kill()
        
class Score(pygame.sprite.Sprite):
    A sprite for the score.
    def __init__(self, xy):
        pygame.sprite.Sprite.__init__(self)
        self.xy = xy    # save xy -- will center our rect on it when we change the score 
        self.font = pygame.font.Font(None, 50)  # load the default font, size 50 
        self.leftscore = 0
        self.rightscore = 0
        self.reRender() 

    def update(self):
        pass
 
    def left(self,n):
        Adds a point to the left side score.
        self.leftscore += n
        self.reRender()
 
    def right(self,n):
        Adds a point to the right side score.
        self.rightscore += n
        self.reRender()
 
    def reset(self):
        Resets the scores to zero.
        self.leftscore = 0
        self.rightscore = 0
        self.reRender()
 
    def reRender(self):
        Updates the score. Renders a new image and re-centers at the initial coordinates.
        self.image = self.font.render("%d     %d"%(self.leftscore, self.rightscore), True, (0,0,0))
        self.rect = self.image.get_rect()
        self.rect.center = self.xy
        
class Wall(pygame.sprite.Sprite):
    This class represents the wall at the top and bottom of the
        screen.
    # Constructor function
    def __init__(self, x, y, width, height):
        # Call the parent's constructor
        super(Wall, self).__init__() 
        # Make a blue wall, of the size specified in the parameters
        self.image = pygame.Surface([width, height])
        self.image.fill((blue)) 
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x

class Barrier(pygame.sprite.Sprite):
    #this will be the place holder for the temporary walls
    def __init__(self, hits):
        super(Barrier, self).__init__()        
        self.hit_num = hits        
        width = random.randint(10,20)
        height = random.randint(50,400)        
        # Make a blue wall, of the size specified in the parameters
        self.image = pygame.Surface([width, height])
        self.image.fill((red))       
        #make a random location
        x = random.randint(50,500)
        y=  random.randint(50,400)        
        #set it in place
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x
    
    #track collisions w wall
    def red_hits(self):
        self.hit_num -= 1       
        if self.hit_num == 0 :
            self.kill()
            
class Portal(pygame.sprite.Sprite):  
    #class for portals in and out
    def __init__(self, x=-1, y=-1):  
        super(Portal, self).__init__()   
        self.x = x
        self.y = y
        self.image = pygame.Surface([10, 10])
        #make sure theyu are hidden
        self.image.fill(blue)
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()         
        # Set initial position of sprite to 100,100
        self.rect.x = x
        self.rect.y = y

class Monster(pygame.sprite.Sprite):
    This class represents the monster that bounces around.
    # Set speed vector
    change_x = 0
    change_y = 0
    walls = None
    barriers = None    
    # Constructor function
    def __init__(self, x, y, walls, players, barriers, change_x = 0, change_y = 0):
        # Call the parent's constructor
        super(Monster, self).__init__()  
        # Set height, width
        self.image = pygame.Surface([50, 50])
        self.image.fill(silver) 
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x         
        self.walls = walls
        self.barriers = barriers        
        self.players = players        
        self.hit_count = 3
         
    def update(self):
        Update the ball's position.
        # Get the old position, in case we need to go back to it
        old_x = self.rect.x
        new_x = old_x + self.change_x
        self.rect.x = new_x         
        # Did this update cause us to hit a wall?
        collide = pygame.sprite.spritecollide(self, self.walls, False)
        if collide:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1            
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5                
        old_y = self.rect.y
        new_y = old_y + self.change_y
        self.rect.y = new_y         
        # Did this update cause us to hit a wall?
        collide = pygame.sprite.spritecollide(self, self.walls, False)
        if collide:
            # Whoops, hit a wall. Go back to the old position
            self.rect.y = old_y
            self.change_y *= -1
        # Did this update cause us to hit a barrier?
        collide_b = pygame.sprite.spritecollide(self, self.barriers, False)
        if collide_b:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1
            self.hit_count += 1
            barrier1.red_hits()
            self.red_hits()           
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5
                self.hit_count = 0        
        # Did this update cause us to hit a wall?
        collide_b = pygame.sprite.spritecollide(self, self.barriers, False)
        if collide_b:
            # Whoops, hit a wall. Go back to the old position
            self.rect.y = old_y
            self.change_y *= -1
            barrier1.red_hits()
            self.red_hits()
               
        if self.rect.x < -100:
            self.kill()            
        elif self.rect.x < 1:
            player2.score += 5
            scoreImage.right(5)            
        elif self.rect.x > screen_width + 100:
            self.kill()            
        elif self.rect.x > screen_width - 1:
            player1.score += 5
            scoreImage.left(5)
            
    def red_hits(self):
        self.hit_count -= 1
        
class Ball(pygame.sprite.Sprite):
     This class represents the ball that bounces around. 
 
    # Set speed vector
    change_x = 0
    change_y = 0
    walls = None
    barriers = None
    monsters = None
    portals = None
    
    # Constructor function
    def __init__(self, x, y, walls, players, barriers, monsters1, monsters2, portals, change_x = 0, change_y = 0):
        # Call the parent's constructor               
        # Set height, width
        self.image = pygame.Surface([15, 15])
        self.image.fill(yellow) 
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x         
        self.walls = walls
        self.barriers = barriers
        self.monsters1 = monsters1
        self.monsters2 = monsters2
        self.portals = portals        
        self.players = players        
        self.hit_count = 0
        super(Ball, self).__init__()
    def reset(self):
        # create ball image
        self.image = pygame.Surface([15, 15])
        self.image.fill(yellow)
        
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()
        # set ball in middle of screen
        self.rect.x = screen_width/2
        self.rect.y = screen_height/2
        #set speed of ball
        self.change_y = random.randrange(-8, 8)
        self.change_x = 8
        #randomly select left/right direction
        if (random.randrange(2) == 0):
            self.change_x *= -1  
    
    def reposition(self,x,y):
        self.x = x
        self.y = y
        self.rect.x = x
        self.rect.y = y
        
        
    def update(self):
         Update the ball's position. 
        # Get the old position, in case we need to go back to it
        old_x = self.rect.x
        new_x = old_x + self.change_x
        self.rect.x = new_x
         
        # Did this update cause us to hit a wall?
        collide = pygame.sprite.spritecollide(self, self.walls, False)
        if collide:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1
            self.hit_count += 1
            
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5
                self.hit_count = 0
                
        old_y = self.rect.y
        new_y = old_y + self.change_y
        self.rect.y = new_y
         
        # Did this update cause us to hit a wall?
        collide = pygame.sprite.spritecollide(self, self.walls, False)
        if collide:
            # Whoops, hit a wall. Go back to the old position
            self.rect.y = old_y
            self.change_y *= -1
             

        # Did this update cause us to hit a barrier?
        collide_b = pygame.sprite.spritecollide(self, self.barriers, False)
        if collide_b:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1
            self.hit_count += 1
            barrier1.red_hits()
            
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5
                self.hit_count = 0
         
        # Did this update cause us to hit a wall?
        collide_b = pygame.sprite.spritecollide(self, self.barriers, False)
        if collide_b:
            # Whoops, hit a wall. Go back to the old position
            self.rect.y = old_y
            self.change_y *= -1
            barrier1.red_hits()
            
        #did we hit the monster?
        collide_m1 = pygame.sprite.spritecollide(self, self.monsters1, False)
        if collide_m1:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1
            self.hit_count += 1
            monster1.red_hits()
            
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5
                self.hit_count = 0 

        collide_m2 = pygame.sprite.spritecollide(self, self.monsters2, False)
        if collide_m2:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1
            self.hit_count += 1
            monster2.red_hits()
            
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5
                self.hit_count = 0 
                
        collide_p = pygame.sprite.spritecollide(self, self.portals, False)
        if collide_p:
            relocate()
            
class Goal(pygame.sprite.Sprite):
    
    
    def __init__(self, x, y, left_right, height, balls, monsters1, monsters2): 
        # Call the parent's constructor
        # Set width and height
        self.image = pygame.Surface([2, height]) #width is equal to 5
        self.image.fill(red)
        
        self.rect = self.image.get_rect() 
        self.rect.y = y
        self.rect.x = x
        
        self.balls = balls
        self.monsters1 = monsters1
        self.monsters2 = monsters2
        
        # left_right is a string that indicates which side the goal is for
        self.left_right = left_right
        super(Goal, self).__init__()
    def update(self):
        collide_ball = pygame.sprite.spritecollide(self, self.balls, False)
        if collide_ball and self.left_right == 'right':
            scoreImage.left(1)
            ball.reset()
            player1.score += 1
        elif collide_ball and self.left_right == 'left':
            scoreImage.right(1)
            ball.reset()
            player2.score += 1
            
        collide_monsters1 = pygame.sprite.spritecollide(self, self.monsters1, False)
        if collide_monsters1 and self.left_right == 'right':
            scoreImage.left(3)
            player1.score += 3
        elif collide_monsters1 and self.left_right == 'left':
            scoreImage.right(3)
            player2.score += 3
        collide_monsters2 = pygame.sprite.spritecollide(self, self.monsters2, False)
        if collide_monsters2 and self.left_right == 'right':
            scoreImage.left(5)
            player1.score += 5
        elif collide_monsters2 and self.left_right == 'left':
            scoreImage.right(5)             
            player2.score += 5


        
                
##########################################################            
#####  OUCHES ####### 
##########################################################

def reset():
    player1.reset(10, screen_height / 2)
    player2.reset(screen_width - 20, screen_height / 2)

#reversed direction
def reverse_paddle_dir(player):
        Player.change_direction(player1)

def reverse_both():
        reverse_paddle_dir(player1)
        reverse_paddle_dir(player2)
 
#make a player green     
def make_green(player):
    Player.make_color(player, green)

#make both paddles green    
def both_paddles_green():
    make_green(player1)
    make_green(player2)

#change size  
def shrink_paddle(player):
    Player.change_size(player, 10, 35)

def expand_paddle(player):
    Player.change_size(player, 10, 150)  

def offset_sizes(small_player, large_player):
    shrink_paddle(small_player)
    expand_paddle(large_player) 
    
def shrink_both_paddles():
    shrink_paddle(player1)
    shrink_paddle(player2)

def relocate():
    ball.reposition(portal_out.x,portal_out.y)
    after they are used, destroy them
    portal_out.kill()
    portal_in.kill()

# lots of balls        
def create_many(bnum):
    for i in range(bnum):
        ball = Ball( -50, -50, wall_list, player_list, barrier_list, monster1_list, monster2_list, portal_list )
        movingsprites.add(ball)
        all_sprites.add(ball)
        ball_group.add(ball)
        # Start in the middle of the screen at a random y location
        ball.rect.x = screen_width/2
        ball.rect.y = random.randrange(10, screen_height - 10)
         
        # Set a random vector
        ball.change_y = random.randrange(-5, 6)
        ball.change_x =  random.randrange(5, 10)
         
        # Is the ball headed left or right? Select randomly
        if( random.randrange(2) == 0 ):
            ball.change_x *= -1 
            
'''
this didn't work, but i think it's an easy fix
disasters = [lambda: reset(), lambda: reverse_paddle_dir(player1), lambda: reverse_paddle_dir(player2),
             reverse_both(), lambda:make_green(player1), lambda: make_green(player2), both_paddles_green(),
            shrink_both_paddles(), lambda: offset_sizes(player1,player2), lambda: offset_sizes(player2,player1),
            lambda: create_many(bnum)]
'''
   
disasters = 1 reset(), 2 reverse_paddle_dir(player1),3 reverse_paddle_dir(player2),
             4 reverse_both(), 5 lambda:make_green(player1), 6 lambda: make_green(player2), 7 both_paddles_green(),
            8 shrink_both_paddles(), 9  lambda: offset_sizes(player1,player2), 10 lambda: offset_sizes(player2,player1),
            11 create_many(bnum)

############################################################################
############################  MAIN LOOP ####################################
############################################################################
               
# Call this function so the Pygame library can initialize itself
pygame.init()
 
# Create an 800x600 sized screen
screen_width = 800
screen_height = 600
 
screen = pygame.display.set_mode([screen_width, screen_height])
 
# Set the title of the window
pygame.display.set_caption("DAVE AND GREG'S CRUEL PONG")
 
# Create a surface we can draw on
background = pygame.Surface(screen.get_size())
 
# Used for converting color maps and such
background = background.convert()
 
# Fill the screen with a black background
background.fill(green)

# score image
scoreImage = Score((400, 50))

        
# All sprite lists
these are used for interasctions in the game such as collisions
ball_group = pygame.sprite.Group()
wall_list = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
movingsprites = pygame.sprite.Group()
player_list = pygame.sprite.Group() # for player 1 and 2 to keep track of hits
barrier_list = pygame.sprite.Group()
monster1_list = pygame.sprite.Group()
monster2_list = pygame.sprite.Group()
portal_list = pygame.sprite.Group()

all_sprites.add(scoreImage)
# Hit count list
#hit_count = 0

# Create the players
player1 = Player(10, screen_height / 2)
player_list.add(player1) # add to list of players for ball collision detection
all_sprites.add(player1)
wall_list.add(player1)
movingsprites.add(player1)
 
player2 = Player(screen_width - 20, screen_height / 2)
player_list.add(player2) # add to list of players for ball collision detection
all_sprites.add(player2)
wall_list.add(player2)
movingsprites.add(player2)
 
# Make the walls. (x_pos, y_pos, width, height)
# Top wall
wall = Wall(0, 0, screen_width, 10) 
wall_list.add(wall)
all_sprites.add(wall)
 
# Bottom wall
wall = Wall(0, screen_height - 10, screen_width, screen_height) 
wall_list.add(wall)
all_sprites.add(wall)
 
# Create the ball
ball = Ball( -50, -50, wall_list, player_list, barrier_list, monster1_list, monster2_list, portal_list )
movingsprites.add(ball)
all_sprites.add(ball)
ball_group.add(ball)

# Create the goals
left_goal = Goal(0,0, 'left', screen_height, ball_group, monster1_list,monster2_list)
right_goal = Goal(screen_width-2, 0, 'right', screen_height, ball_group, monster1_list, monster2_list)
all_sprites.add(left_goal)
movingsprites.add(left_goal)
all_sprites.add(right_goal) 
movingsprites.add(right_goal)

# create initial monster
monster2 = Monster2(wall_list, ball_group)
all_sprites.add(monster2)
movingsprites.add(monster2)
monster2_list.add(monster2)
 
clock = pygame.time.Clock()
 
done = False

#stuff we aded to makethe game roll better

#which ouch is currently applied
ouch_num = 0
bad_time = random.randint(5,15)
loop_count = 1

# Main program loop
while not done:
     
    # Loop through any window events
    for event in pygame.event.get():
        # The user clicked 'close' or hit Alt-F4
        if event.type == pygame.QUIT:
            done = True
        # The user clicked the mouse button
        # or pressed a key
        
        
        # Is the ball not moving?
        if ball.change_y == 0:            
            #reset our add ons
            ouch_num = 0
            reset()            
            # Start in the middle of the screen at a random y location
            ball.rect.x = screen_width/2
            ball.rect.y = random.randrange(10, screen_height - 10)             
            # Set a random vector
            ball.change_y = random.randrange(-5, 6)
            ball.change_x =  random.randrange(5, 10)             
            # Is the ball headed left or right? Select randomly
            if( random.randrange(2) == 0 ):
                ball.change_x *= -1
        
        this is used to read the player UI, also this is the part we need to send
        to the network
        if event.type == pygame.KEYDOWN:
            # Control player2 movement with up/down keys for KEYDOWN event
            if event.key == pygame.K_UP:
                player2.changespeed(-player2.speed) # because pixels are going down screen
            elif event.key == pygame.K_DOWN:
                player2.changespeed(player2.speed)
            # Control player 1 movement with w/s keys for KEYDOWN event
            if event.key == pygame.K_w:
                player1.changespeed(-player1.speed)
            elif event.key == pygame.K_s:
                player1.changespeed(player1.speed)                
            #add ability to send disaters to players   
            if event.key == pygame.K_d:
                #shrink the first argument, expand the second 
                offset_sizes(player1, player2)
            elif event.key == pygame.K_l:
                offset_sizes(player2, player1)
            elif event.key == pygame.K_r:
                #reset the paddles
                reset()
            #SUPER PADDLE!!!! 
            elif event.key == pygame.K_v:
                p = random.randint(0,100)
                if p%2 == 0:
                    Player.change_size(player1,10, 275,10, screen_height / 2)
                elif p%2 == 1:
                    Player.change_size(player2,10, 275,screen_width - 20, screen_height / 2)
                
        elif event.type == pygame.KEYUP:
            # Control player 2 movement with w/s keys for KEYUP event
            if event.key == pygame.K_UP:
                player2.changespeed(player2.speed)
            elif event.key == pygame.K_DOWN:
                player2.changespeed(-player2.speed)                
            # Control player 1 movement with w/s keys for KEYUP event
            if event.key == pygame.K_w:
                player1.changespeed(player1.speed)
            elif event.key == pygame.K_s:
                player1.changespeed(-player1.speed)
       
    #Disaster anyone???   
    if ouch_num == 0 and loop_count%3000 == 0: #10 sec timer)
        ouch_num = random.randint(0,7)
        #make a set of functions and list then shuffle the list        
        if ouch_num == 1:
            #shrink the paddles
            shrink_both_paddles()
        elif ouch_num == 2:
            #make the paddles invisible by coloring green
            both_paddles_green()
        elif ouch_num == 3:
            #add a boat load of extra balls
            bnum = random.randint(1,11)
            create_many(bnum)
        elif ouch_num == 4:
            #reverse the direction the keys move (buggy)
            reverse_both()
        elif ouch_num == 5:
            #make the ball move REALLY fast
            ball.change_x *= 4
            ball.change_y *= 4
        elif ouch_num == 6:
            #random wall with random hit points
            hn = random.randint(1,5)
            barrier1 = Barrier(hn)
            barrier_list.add(barrier1)
            all_sprites.add(barrier1)
        elif ouch_num == 7:
            #create monster with hit points and worth 5 points
            monster1 = Monster(screen_width/2,random.randrange(10, screen_height - 10),wall_list,player_list,barrier_list)
            #random speed            
            monster1.change_y = random.randrange(-5, 6)
            monster1.change_x =  random.randrange(5, 10)
            #random direction            
            if( random.randrange(2) == 0 ):
                monster1.change_x *= -1
            movingsprites.add(monster1)
            monster1_list.add(monster1)
            all_sprites.add(monster1)
        elif ouch_num == 8:
            #create portal in in a random spot
            pi_x = random.randint(10,screen_width - 10)
            pi_y = random.randint(10 , screen_height - 10)
            portal_in = Portal(pi_x,pi_y)
            portal_list.add(portal_in)
            all_sprites.add(portal_in)
            #create protal out randomly, ensure not in same spot as portal in
            po_x = random.randint(10,screen_width - 10)
            po_y = random.randint(10 , screen_height - 10)
            #make sure they aren't in the same spot
            while po_x == pi_x:
                po_x = random.randint(10,screen_width - 10)
            while po_y == pi_y:
                po_y = random.randint(10 , screen_height - 10)     
            portal_out = Portal(po_x,po_y)        
            portal_list.add(portal_out)         
            all_sprites.add(portal_out)
        ### end of obsticles ######
              
    # Update the ball position. Pass it the list of stuff it can bounce off of
    movingsprites.update()    
    # Clear the screen
    screen.fill(green)     
    # Draw the sprites
    all_sprites.draw(screen) 
    # Display the screen
    pygame.display.flip()    
    #make sure not to make ouches too often
    loop_count += 1
    clock.tick(30)

print the score when it's all done
print player1.score
print player2.score
# All done, shut down Pygame            
pygame.quit()


##### two methods to network for future work: 
# 1 server for math that does all the work
# 2 coordinate 2 different pygames
"""
import pygame
import random
import os 

black = (0, 0, 0)
white = (255, 255, 255)
blue =  (0, 0, 255)
red = (255, 0, 0)
green = (0, 255, 0)
yellow = (255, 255, 0)
silver = (192, 192, 192)
 
# Define CLASSES below:
# Player class is for the paddles on each side
# Player class is derived from sprite class in Pygame
class Player(pygame.sprite.Sprite):
    # Player shape
    width = 10
    height = 75    
    # Set speed vector (Player only moves in y-direction)
    change_y = 0
    walls = None   
    # Constructor. Pass in the color of the block, and its x and y position
    def __init__(self, x, y,width = 10, height = 75):
        # Call the parent class (Sprite) constructor
        super(Player, self).__init__()  
        # Variables to hold the height and width of the block
        # Create an image of the ball, and fill it with a color.
        # This could also be an image loaded from the disk.
        self.image = pygame.Surface([self.width, self.height])
        self.image.fill(white)
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()        
        # Set initial position of sprite to 100,100
        self.rect.x = x
        self.rect.y = y
        self.x = x
        self.y = y
        self.speed = 10
        self.score = 0
        
    def changespeed(self, y):
        """ Change the speed of the player. """
        self.change_y += y        
         
    def make_color(self, color):
        self.image.fill(color)

    def update(self):
        """ Update the player's position. """
        # Move up/down
        self.rect.y += self.change_y
        self.y += self.change_y
        # If the user moves past the top/bottom of the screen, set the position
        # to the edge.
        if self.rect.y < 0:
                self.rect.y = 0
                self.y = 0
        if self.rect.y > screen_height - self.height:
                self.rect.y = screen_height - self.height
                self.y = screen_height - self.height
        
    def change_size(self, width, height): 
        self.image = pygame.Surface([width, height])
        self.image.fill(white)
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()
         # Set initial position of sprite to 100,100
        self.rect.x = self.x
        self.rect.y = self.y

    def change_direction(self):
        #cause the direction to reverse
        self.speed *= -1

    def reset(self, x ,y):
        self.width = 10
        self.height = 75         
        self.image = pygame.Surface([self.width, self.height])
        self.image.fill(white)
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()        
        # Set initial position of sprite to 100,100
        self.rect.x = x
        self.rect.y = y      
        self.speed = 10

class Monster2(pygame.sprite.Sprite):
    """ This class represents the monster that bounces around. """
    # Set speed vector
    change_x = 0
    change_y = 0
    walls = None
    barriers = None
    
    # Constructor function
    def __init__(self, walls, balls):
        # Call the parent's constructor
        super(Monster2, self).__init__()
        # Set height, width
#        self.image = pygame.image.load(os.path.join('images','err.gif'))
        self.image = pygame.Surface([100, 100])
        self.image.fill(silver)
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.y = screen_height/2
        self.rect.x = 200       
        # pass is objects monster will react with
        self.walls = walls
        self.balls = balls       
        self.change_x = 0
        self.change_y = 8        
        self.hit_count = 4       
        self.loop = 0
        
    def update(self):
        # Get the old position, in case we need to go back to it
        old_x = self.rect.x
        new_x = old_x + self.change_x
        self.rect.x = new_x        
        old_y = self.rect.y
        new_y = old_y + self.change_y
        self.rect.y = new_y
               
        # when monster collides with wall, reverse direction
        collide_wall = pygame.sprite.spritecollide(self, self.walls, False)
        if collide_wall:
            self.rect.y = old_y
            self.change_y *= -1
            self.loop += 1             
        if (self.loop % 2) == 1:
            if self.change_y < 0 and self.rect.y > screen_height/2:
                self.change_y = 0
                if self.rect.x > screen_width/2:
                    self.change_x = -5
                else:
                    self.change_x = 5
        # trying to get monster to move left and right            
        if self.change_y == 0 and self.rect.x > screen_width/2 + 150:
            self.change_x = 0
            self.change_y = -5
#        elif self.change_y == 0 and self.rect.x < screen_width/2 - 150:
#            self.change_x = 0
#            self.change_y = -5
            
    def red_hits(self):
        self.hit_count -= 1        
        if self.hit_count == 0:
            self.kill()
        
class Score(pygame.sprite.Sprite):
    """A sprite for the score."""
    def __init__(self, xy):
        pygame.sprite.Sprite.__init__(self)
        self.xy = xy    # save xy -- will center our rect on it when we change the score 
        self.font = pygame.font.Font(None, 50)  # load the default font, size 50 
        self.leftscore = 0
        self.rightscore = 0
        self.reRender() 

    def update(self):
        pass
 
    def left(self,n):
        """Adds a point to the left side score."""
        self.leftscore += n
        self.reRender()
 
    def right(self,n):
        """Adds a point to the right side score."""
        self.rightscore += n
        self.reRender()
 
    def reset(self):
        """Resets the scores to zero."""
        self.leftscore = 0
        self.rightscore = 0
        self.reRender()
 
    def reRender(self):
        """Updates the score. Renders a new image and re-centers at the initial coordinates."""
        self.image = self.font.render("%d     %d"%(self.leftscore, self.rightscore), True, (0,0,0))
        self.rect = self.image.get_rect()
        self.rect.center = self.xy
        
class Wall(pygame.sprite.Sprite):
    """ This class represents the wall at the top and bottom of the
        screen. """
    # Constructor function
    def __init__(self, x, y, width, height):
        # Call the parent's constructor
        super(Wall, self).__init__() 
        # Make a blue wall, of the size specified in the parameters
        self.image = pygame.Surface([width, height])
        self.image.fill((blue)) 
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x

class Barrier(pygame.sprite.Sprite):
    #this will be the place holder for the temporary walls
    def __init__(self, hits):
        super(Barrier, self).__init__()        
        self.hit_num = hits        
        width = random.randint(10,20)
        height = random.randint(50,400)        
        # Make a blue wall, of the size specified in the parameters
        self.image = pygame.Surface([width, height])
        self.image.fill((red))       
        #make a random location
        x = random.randint(50,500)
        y=  random.randint(50,400)        
        #set it in place
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x
    
    #track collisions w wall
    def red_hits(self):
        self.hit_num -= 1       
        if self.hit_num == 0 :
            self.kill()
            
class Portal(pygame.sprite.Sprite):  
    #class for portals in and out
    def __init__(self, x=-1, y=-1):  
        super(Portal, self).__init__()   
        self.x = x
        self.y = y
        self.image = pygame.Surface([10, 10])
        #make sure theyu are hidden
        self.image.fill(blue)
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()         
        # Set initial position of sprite to 100,100
        self.rect.x = x
        self.rect.y = y

class Monster(pygame.sprite.Sprite):
    """ This class represents the monster that bounces around. """
    # Set speed vector
    change_x = 0
    change_y = 0
    walls = None
    barriers = None    
    # Constructor function
    def __init__(self, x, y, walls, players, barriers, color, change_x = 0, change_y = 0):
        # Call the parent's constructor
        super(Monster, self).__init__()  
        # Set height, width
        self.image = pygame.Surface([50, 50])
        self.image.fill(color) 
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x         
        self.walls = walls
        self.barriers = barriers        
        self.players = players        
        self.hit_count = 3
         
    def update(self):
        """ Update the ball's position. """
        # Get the old position, in case we need to go back to it
        old_x = self.rect.x
        new_x = old_x + self.change_x
        self.rect.x = new_x         
        # Did this update cause us to hit a wall?
        collide = pygame.sprite.spritecollide(self, self.walls, False)
        if collide:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1            
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5                
        old_y = self.rect.y
        new_y = old_y + self.change_y
        self.rect.y = new_y         
        # Did this update cause us to hit a wall?
        collide = pygame.sprite.spritecollide(self, self.walls, False)
        if collide:
            # Whoops, hit a wall. Go back to the old position
            self.rect.y = old_y
            self.change_y *= -1
        # Did this update cause us to hit a barrier?
        collide_b = pygame.sprite.spritecollide(self, self.barriers, False)
        if collide_b:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1
            self.hit_count += 1
            barrier1.red_hits()
            self.red_hits()           
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5
                self.hit_count = 0        
        # Did this update cause us to hit a wall?
        collide_b = pygame.sprite.spritecollide(self, self.barriers, False)
        if collide_b:
            # Whoops, hit a wall. Go back to the old position
            self.rect.y = old_y
            self.change_y *= -1
            barrier1.red_hits()
            self.red_hits()
            
    def red_hits(self):
        self.hit_count -= 1
        
class Ball(pygame.sprite.Sprite):
    """ This class represents the ball that bounces around. """
 
    # Set speed vector
    change_x = 0
    change_y = 0
    walls = None
    barriers = None
    monsters = None
    portals = None
    
    # Constructor function
    def __init__(self, x, y, walls, players, barriers, monsters1, monsters2, portals, change_x = 0, change_y = 0):
        # Call the parent's constructor               
        # Set height, width
        self.image = pygame.Surface([15, 15])
        self.image.fill(yellow) 
        # Make our top-left corner the passed-in location.
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x         
        self.walls = walls
        self.barriers = barriers
        self.monsters1 = monsters1
        self.monsters2 = monsters2
        self.portals = portals        
        self.players = players        
        self.hit_count = 0
        super(Ball, self).__init__()
    def reset(self):
        # create ball image
        self.image = pygame.Surface([15, 15])
        self.image.fill(yellow)
        
        # Fetch the rectangle object that has the dimensions of the image
        self.rect = self.image.get_rect()
        # set ball in middle of screen
        self.rect.x = screen_width/2
        self.rect.y = screen_height/2
        #set speed of ball
        self.change_y = random.randrange(-8, 8)
        self.change_x = 8
        #randomly select left/right direction
        if (random.randrange(2) == 0):
            self.change_x *= -1  
    
    def reposition(self,x,y):
        self.x = x
        self.y = y
        self.rect.x = x
        self.rect.y = y
        
        
    def update(self):
        """ Update the ball's position. """
        # Get the old position, in case we need to go back to it
        old_x = self.rect.x
        new_x = old_x + self.change_x
        self.rect.x = new_x
         
        # Did this update cause us to hit a wall?
        collide = pygame.sprite.spritecollide(self, self.walls, False)
        if collide:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1
            self.hit_count += 1
            
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5
                self.hit_count = 0
                
        old_y = self.rect.y
        new_y = old_y + self.change_y
        self.rect.y = new_y
         
        # Did this update cause us to hit a wall?
        collide = pygame.sprite.spritecollide(self, self.walls, False)
        if collide:
            # Whoops, hit a wall. Go back to the old position
            self.rect.y = old_y
            self.change_y *= -1
             

        # Did this update cause us to hit a barrier?
        collide_b = pygame.sprite.spritecollide(self, self.barriers, False)
        if collide_b:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1
            self.hit_count += 1
            barrier1.red_hits()
            
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5
                self.hit_count = 0
         
        # Did this update cause us to hit a wall?
        collide_b = pygame.sprite.spritecollide(self, self.barriers, False)
        if collide_b:
            # Whoops, hit a wall. Go back to the old position
            self.rect.y = old_y
            self.change_y *= -1
            barrier1.red_hits()
            
        #did we hit the monster?
        collide_m1 = pygame.sprite.spritecollide(self, self.monsters1, False)
        if collide_m1:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1
            self.hit_count += 1
            monster1.red_hits()
            
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5
                self.hit_count = 0 

        collide_m2 = pygame.sprite.spritecollide(self, self.monsters2, False)
        if collide_m2:
            # Whoops, hit a wall. Go back to the old position
            self.rect.x = old_x
            self.change_x *= -1
            self.hit_count += 1
            monster2.red_hits()
            
            #like in the real game, if the ball hits so many times
            #then make it bounce faster
            if self.hit_count > 7:
                self.change_x *= 1.5
                self.change_y *= 1.5
                self.hit_count = 0 
                
        collide_p = pygame.sprite.spritecollide(self, self.portals, False)
        if collide_p:
            relocate()
            
class Goal(pygame.sprite.Sprite):
    
    
    def __init__(self, x, y, left_right, height, balls, monsters1, monsters2): 
        # Call the parent's constructor
        # Set width and height
        self.image = pygame.Surface([2, height]) #width is equal to 5
        self.image.fill(red)
        
        self.rect = self.image.get_rect() 
        self.rect.y = y
        self.rect.x = x
        
        self.balls = balls
        self.monsters1 = monsters1
        self.monsters2 = monsters2
        
        # left_right is a string that indicates which side the goal is for
        self.left_right = left_right
        super(Goal, self).__init__()
    def update(self):
        collide_ball = pygame.sprite.spritecollide(self, self.balls, False)
        if collide_ball and self.left_right == 'right':
            scoreImage.left(1)
            ball.reset()
            player1.score += 1
        elif collide_ball and self.left_right == 'left':
            scoreImage.right(1)
            ball.reset()
            player2.score += 1
            
        collide_monsters1 = pygame.sprite.spritecollide(self, self.monsters1, False)
        if collide_monsters1 and self.left_right == 'right':
            scoreImage.left(1)
            player1.score += 1
        elif collide_monsters1 and self.left_right == 'left':
            scoreImage.right(1)
            player2.score += 1
        collide_monsters2 = pygame.sprite.spritecollide(self, self.monsters2, False)
        if collide_monsters2 and self.left_right == 'right':
            scoreImage.left(5)
            player1.score += 5
        elif collide_monsters2 and self.left_right == 'left':
            scoreImage.right(5)             
            player2.score += 5


        
                
##########################################################            
#####  OUCHES ####### 
##########################################################

def reset():
    player1.reset(10, screen_height / 2)
    player2.reset(screen_width - 20, screen_height / 2)

#reversed direction
def reverse_paddle_dir(player):
        Player.change_direction(player1)

def reverse_both():
        reverse_paddle_dir(player1)
        reverse_paddle_dir(player2)
 
#make a player green     
def make_green(player):
    Player.make_color(player, green)

#make both paddles green    
def both_paddles_green():
    make_green(player1)
    make_green(player2)

#change size  
def shrink_paddle(player):
    Player.change_size(player, 10, 35)

def expand_paddle(player):
    Player.change_size(player, 10, 150)  

def offset_sizes(small_player, large_player):
    shrink_paddle(small_player)
    expand_paddle(large_player) 
    
def shrink_both_paddles():
    shrink_paddle(player1)
    shrink_paddle(player2)

def relocate():
    ball.reposition(portal_out.x,portal_out.y)
    portal_out.kill()
    portal_in.kill()

def speed_up():
    ball.change_x *= 4
    ball.change_y *= 4
    
# lots of balls        
def create_many():
    bnum = random.randint(1,11)
    for i in range(bnum):
        ball = Ball( -50, -50, wall_list, player_list, barrier_list, monster1_list, monster2_list, portal_list )
        movingsprites.add(ball)
        all_sprites.add(ball)
        ball_group.add(ball)
        # Start in the middle of the screen at a random y location
        ball.rect.x = screen_width/2
        ball.rect.y = random.randrange(10, screen_height - 10)
         
        # Set a random vector
        ball.change_y = random.randrange(-5, 6)
        ball.change_x =  random.randrange(5, 10)
         
        # Is the ball headed left or right? Select randomly
        if( random.randrange(2) == 0 ):
            ball.change_x *= -1 
            

#this didn't work, but i think it's an easy fix
#disasters = [lambda: reset(), lambda: reverse_paddle_dir(player1), lambda: reverse_paddle_dir(player2),
#             reverse_both(), lambda:make_green(player1), lambda: make_green(player2), both_paddles_green(),
#            shrink_both_paddles(), lambda: offset_sizes(player1,player2), lambda: offset_sizes(player2,player1),
#            lambda: create_many(bnum)]


############################################################################
############################  MAIN LOOP ####################################
############################################################################
               
# Call this function so the Pygame library can initialize itself
pygame.init()
 
# Create an 800x600 sized screen
screen_width = 800
screen_height = 600
 
screen = pygame.display.set_mode([screen_width, screen_height])
 
# Set the title of the window
pygame.display.set_caption("DAVE AND GREG'S CRUEL PONG")
 
# Create a surface we can draw on
background = pygame.Surface(screen.get_size())
 
# Used for converting color maps and such
background = background.convert()
 
# Fill the screen with a black background
background.fill(green)

# score image
scoreImage = Score((400, 50))

        
# All sprite lists
ball_group = pygame.sprite.Group()
wall_list = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
movingsprites = pygame.sprite.Group()
player_list = pygame.sprite.Group() # for player 1 and 2 to keep track of hits
barrier_list = pygame.sprite.Group()
monster1_list = pygame.sprite.Group()
monster2_list = pygame.sprite.Group()
portal_list = pygame.sprite.Group()

all_sprites.add(scoreImage)
# Hit count list
#hit_count = 0

# Create the players
player1 = Player(10, screen_height / 2)
player_list.add(player1) # add to list of players for ball collision detection
all_sprites.add(player1)
wall_list.add(player1)
movingsprites.add(player1)
 
player2 = Player(screen_width - 20, screen_height / 2)
player_list.add(player2) # add to list of players for ball collision detection
all_sprites.add(player2)
wall_list.add(player2)
movingsprites.add(player2)
 
# Make the walls. (x_pos, y_pos, width, height)
# Top wall
wall = Wall(0, 0, screen_width, 10) 
wall_list.add(wall)
all_sprites.add(wall)
 
# Bottom wall
wall = Wall(0, screen_height - 10, screen_width, screen_height) 
wall_list.add(wall)
all_sprites.add(wall)
 
# Create the ball
ball = Ball( -50, -50, wall_list, player_list, barrier_list, monster1_list, monster2_list, portal_list )
movingsprites.add(ball)
all_sprites.add(ball)
ball_group.add(ball)

# Create the goals
left_goal = Goal(0,0, 'left', screen_height, ball_group, monster1_list,monster2_list)
right_goal = Goal(screen_width-2, 0, 'right', screen_height, ball_group, monster1_list, monster2_list)
all_sprites.add(left_goal)
movingsprites.add(left_goal)
all_sprites.add(right_goal) 
movingsprites.add(right_goal)

# create initial monster
monster2 = Monster2(wall_list, ball_group)
all_sprites.add(monster2)
movingsprites.add(monster2)
monster2_list.add(monster2)
 
clock = pygame.time.Clock()
 
done = False

#stuff we aded to makethe game roll better

#which ouch is currently applied
ouch_num = 0
bad_time = random.randint(5,15)
loop_count = 1

# Main program loop
while not done:
     
    # Loop through any window events
    for event in pygame.event.get():
        # The user clicked 'close' or hit Alt-F4
        if event.type == pygame.QUIT:
            done = True
        # The user clicked the mouse button
        # or pressed a key
        
        
        # Is the ball not moving?
        if ball.change_y == 0:            
            #reset our add ons
            ouch_num = 0
            reset()            
            # Start in the middle of the screen at a random y location
            ball.rect.x = screen_width/2
            ball.rect.y = random.randrange(10, screen_height - 10)             
            # Set a random vector
            ball.change_y = random.randrange(-5, 6)
            ball.change_x =  random.randrange(5, 10)             
            # Is the ball headed left or right? Select randomly
            if( random.randrange(2) == 0 ):
                ball.change_x *= -1
        
        if event.type == pygame.KEYDOWN:
            # Control player2 movement with up/down keys for KEYDOWN event
            if event.key == pygame.K_UP:
                player2.changespeed(-player2.speed) # because pixels are going down screen
            elif event.key == pygame.K_DOWN:
                player2.changespeed(player2.speed)
            # Control player 1 movement with w/s keys for KEYDOWN event
            if event.key == pygame.K_w:
                player1.changespeed(-player1.speed)
            elif event.key == pygame.K_s:
                player1.changespeed(player1.speed)                
            #add ability to send disaters to players   
            if event.key == pygame.K_d:
                #shrink the first argument, expand the second 
                offset_sizes(player1, player2)
            elif event.key == pygame.K_l:
                offset_sizes(player2, player1)
            elif event.key == pygame.K_r:
                #reset the paddles
                reset()
            #SUPER PADDLE!!!! 
            elif event.key == pygame.K_v:
                p = random.randint(0,100)
                if p%2 == 0:
                    Player.change_size(player1,10, 275,10, screen_height / 2)
                elif p%2 == 1:
                    Player.change_size(player2,10, 275,screen_width - 20, screen_height / 2)
                
        elif event.type == pygame.KEYUP:
            # Control player 2 movement with w/s keys for KEYUP event
            if event.key == pygame.K_UP:
                player2.changespeed(player2.speed)
            elif event.key == pygame.K_DOWN:
                player2.changespeed(-player2.speed)                
            # Control player 1 movement with w/s keys for KEYUP event
            if event.key == pygame.K_w:
                player1.changespeed(player1.speed)
            elif event.key == pygame.K_s:
                player1.changespeed(-player1.speed)
       
    #Disaster anyone??? 
    """
        I want to replace this list of elifs with a function definition list
        but for now this prevents me from making the disasters that initialize a class work
    """
    if ouch_num == 0 and loop_count%30 == 0: #10 sec timer)
        ouch_num = random.randint(0,8)
        #make a set of functions and list then shuffle the list        
        if ouch_num == 0:
            #palyers got lucky, not disasters
            pass
        elif ouch_num == 1:
            #shrink the paddles
            shrink_both_paddles()
        elif ouch_num == 2:
            #make the paddles invisible by coloring green
            both_paddles_green()
        elif ouch_num == 3:
            #add a boat load of extra balls
            create_many()
        elif ouch_num == 4:
            #reverse the direction the keys move (buggy)
            reverse_both()
        elif ouch_num == 5:
            #make the ball move REALLY fast
            speed_up()
        elif ouch_num == 6:
            #random wall with random hit points
            hn = random.randint(1,5)
            barrier1 = Barrier(hn)
            barrier_list.add(barrier1)
            all_sprites.add(barrier1)
        elif ouch_num == 7:
            #create monster with hit points and worth 5 points
            monster1 = Monster(screen_width/2,random.randrange(10, screen_height - 10),wall_list,player_list,barrier_list, silver)
            #random speed            
            monster1.change_y = random.randrange(-5, 6)
            monster1.change_x =  random.randrange(5, 10)
            #random direction            
            if( random.randrange(2) == 0 ):
                monster1.change_x *= -1
            movingsprites.add(monster1)
            monster1_list.add(monster1)
            all_sprites.add(monster1)
        elif ouch_num == 8:
            #create portal in in a random spot
            pi_x = random.randint(10,screen_width - 10)
            pi_y = random.randint(10 , screen_height - 10)
            portal_in = Portal(pi_x,pi_y)
            portal_list.add(portal_in)
            all_sprites.add(portal_in)
            #create protal out randomly, ensure not in same spot as portal in
            po_x = random.randint(10,screen_width - 10)
            po_y = random.randint(10 , screen_height - 10)
            #make sure they aren't in the same spot
            while po_x == pi_x:
                po_x = random.randint(10,screen_width - 10)
            while po_y == pi_y:
                po_y = random.randint(10 , screen_height - 10)     
            portal_out = Portal(po_x,po_y)        
            portal_list.add(portal_out)         
            all_sprites.add(portal_out)
        ### end of obsticles ######
              
    # Update the ball position. Pass it the list of stuff it can bounce off of
    movingsprites.update()    
    # Clear the screen
    screen.fill(green)     
    # Draw the sprites
    all_sprites.draw(screen) 
    # Display the screen
    pygame.display.flip()    
    #make sure not to make ouches too often
    loop_count += 1
    clock.tick(30)

print player1.score
print player2.score
# All done, shut down Pygame            
pygame.quit()


##### two methods to network
# 1 server for math that does all the work
# 2 coordinate 2 different pygames
