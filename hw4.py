# -*- coding: utf-8 -*-
"""
Created on Thu Oct 30 21:35:35 2014

@author: gregkline dave westwood

//PSEUDO CODE //
goals = -2
permanent occupancy (ball, paddle, borders = -1)
regular occupancy defined by hits to remove (temp wall 3 needs hit 3 times)


class game_world
    class GameBoard(object) 
        __init__(self, height = 480, width == 620, p = 2)
            self.Board = np.array([height, width])
            self.height = height
            self.width = width
            
            self.Board = border(self.Board, p)
        
        def border (self, p):
            add a border  (elements = -1)
        
        def goal
            add goals (elements = -2)
            
    class Ball(object)
        __init__(self,x=0,y=0,dx = 0,dy =0, r=2)
            self.x = x #x pos
            self.y = y #y pos
            self.dx = dx #x vel
            self.dy = dy #y vel
            self.r = r #radius
            self.bounce_num = 0 #count the bounces to add increased speed
            
            self.Ball = np.array([2*r,2*r])
            self.Ball = Fill(self.Ball)
            
        def Fill(obj)
            Make a circle of points inside matrix (elements = -1)
            
        def Move(obj)
            x = x + dx
            y = y + dy
            etc  use to move all the matrix elements and thus move the ball
            
    class Paddle(object)
        __init__(self, x=0,y=0,shape = 'rect',l=10,w=2)
            self.x = x
            self.y = y
            self.dx = 0
            self.dy = 0    
            self.l = 10 #length
            self.w = 2 #width
            self.shape = 'rect' #default to rect
            self.paddle = Build(self.shape)
            
        def Move(obj)
            x = x + dx
            y = y + dy
            
        def Build(obj)
            create matrix that will define the paddle using shape criteria
            and l and w criteria
            
    class Ouch(object)
        __init__(self)
            self.ouch = 'none'
            
        def wall_me(obj, x, y, h = 1, l = 5, w = 2)
            W  = np.zeros([l,w]) #initialize an array of size lxw
            W = W + h #add the hit numbers
            
            gameboard = gameboard + W  #add the array to the gameboard
            
        def obsticle 2
        
        ...
        ...
        ...
    
    class Player(object)
        def __init__ (self, up_button, down_button, Name = 'Player', Score = 0)
            self.name = Name
            self.score = Score
            self.up_button = up_button
            self.down_button = down_button
            #put this in for a thing to let players store an attack
            self.special = 'none'
            
        def change_score(self,ds = 0)
            self.score = self.score + ds
        
        
def Game_Start():
    board = GameBoard(480,620,2)
    ball = Ball(240,320,np.rand(),np.rand(),3)
    paddle1 = Paddle(240,20,'rect',10,2)
    paddle2 = Paddle(240,600,'rect',10,2)
    Border(board, 3)
    Goal(board,ends)   
    
    frame = build_frame(board, paddle1,paddle2,ball)
    return frame
    
def Buold_frame():
    piece together all the parts to create a frame 
    
    frame = board
    frame = frame + paddle
    frame = frame + ball

    return frame 
        
def Main()
    Game_Start()
    
    loop:
        search for events (motion keys)
        update paddle locations        
        move the ball
            check for speed increases
        run a random number generator and set triggers for ouches
            add ouch if time
        update frame
        create output matrix
        print to screen
        

    
"""
import numpy as np
import cmath as cm
import Tkinter as tk
from PIL import Image, ImageTk
import matplotlib.pyplot as plt
import matplotlib.cm as colm

class Game_World(object):
    #high score list
    def __init__(self):
        self.high_scores = [];
    
    #update high scores
    def Update_Highs(self,player,score):
        self.high_scores.append(player, score)
        self.high_scores.sort()
        
    class GameBoard(object): 
        def __init__(self, height = 480, width = 620, p = 5):
            #initialize the occupancy grid as 0's
            self.board = np.zeros([height, width])
            self.board = self.board - 3
            self.height = height
            self.width = width
            self.b_width = p
            #apply build functions
            self.board = self.Border_build(p)
            self.board = self.Goal_build(480)
                      
        def Border_build(self, p):
            #add a border  (elements = -1)
            #boarder top rows and left columns
            #p = self.p
            wval = -2.1
            for i in range(p-1):
                self.board[:,i] = wval
                self.board[i,:] = wval
            #add boarder for right and bottom
            for j in range(p-1):
                self.board[:,-1-j] = wval
                self.board[-1-j,:] = wval               
            return self.board
            
        def Goal_build(self, g_width = 480):
            #add goals (elements = -2)
            g = int(round(g_width/2))            
            h = int(round(self.height/2))
            p = self.b_width
            gval = 1
            for i in range(g):
                for j in range(p-1):
                    self.board[h+i,j] = gval
                    self.board[h-i,j] = gval
                    self.board[h+i,-1-j] = gval
                    self.board[h-i,-1-j] = gval
            return self.board 
            
    class Ball(object):
        def __init__(self,x=0,y=0,dx = 0,dy =0, r=5):
            self.x = x #x pos
            self.y = y #y pos
            self.dx = dx #x vel
            self.dy = dy #y vel
            self.r = r #radius
            self.bounce_num = 0 #count the bounces to add increased speed
            
            self.ball = np.zeros([2*r+1,2*r+1])
            self.ball = self.ball - 3
            self.ball = self.Fill_ball()
            
        def Fill_ball(self):
            #Make a circle of points inside matrix (elements = -1)
            r = self.r
            x = r+1
            y = r+1
            bval = 4.2
            self.ball[x,y]= bval
            for i in range(r):
                for j in range(r):
                    if cm.sqrt(i**2+j**2).real <= r:
                        self.ball[x+i-1,y+j-1] = bval
                        self.ball[x-i-1,y-j-1] = bval
                        self.ball[x-i-1,y+j-1] = bval
                        self.ball[x+i-1,y-j-1] = bval
            return self.ball
            
        def Move_ball(self,dx,dy):
            self.x = self.x + dx
            self.y = self.y + dy
            #etc  use to move all the matrix elements and thus move the ball
            return self.x, self.y
            
    class Paddle(object):
        def __init__(self, x=0,y=0,shape = 'rect',l=100,w=3):
            self.x = x
            self.y = y
            self.dx = 0
            self.dy = 0    
            self.l = l #length
            self.w = w #width
            self.shape = 'rect' #default to rect
            self.paddle = self.Build()
            
        def Move_Paddle(self,dx,dy):
            #dx, dy defined in pixels
            self.x = self.x + dx
            self.y = self.y + dy
            return self.x, self.y
            
        def Build(self):
            #create matrix that will define the paddle using shape criteria
            #and l and w criteria
            self.paddle = np.zeros([self.l,self.w])
            self.paddle = self.paddle - 2.1
            
    class Ouch(object):
        def __init__(self):
            self.ouch = 'none'
            
        def wall_me(obj, x, y, h = 1, l = 5, w = 2):
            W  = np.zeros([l,w]) #initialize an array of size lxw
            W = W + h #add the hit numbers
            
            #gameboard = gameboard + W  #add the array to the gameboard
            #return gameboard
            
        #def obsticle 2
        
        #...
        #...
        #...
    class Player(object):
        def __init__ (self, up_button, down_button, Name = 'Player', Score = 0):
            self.name = Name
            self.score = Score
            self.up_button = up_button
            self.down_button = down_button
            #put this in for a thing to let players store an attack
            self.special = 'none'
            
        def change_score(self,ds = 0):
            self.score = self.score + ds

class Game_window(object):
    def __init__(self):
        self.root = tk.Tk()
        self.frame = tk.Frame(self.root, width = 620, height = 480) 
        self.frame.pack()
        self.canvas = tk.Canvas(self.frame, width = 620, height = 480)
        self.canvas.place(x=-2,y=-2)
     
    def Update_window(self,frame):
        self.fr = Image.fromarray(frame, 'RGB')  
        self.ph = ImageTk.PhotoImage(image = self.fr)
        self.canvas.create_image(0,0,image=self.ph)
        self.root.update()
        
#def Game_Start():
#    board_1 = Game_World.GameBoard(480,620,20)
#    #Game_World.GameBoard.Border_build(board_1, 5)
#    Game_World.GameBoard.Goal_build(board_1, 480)  
#    ball_1 = Game_World.Ball(240,320,0,0,10)
#    paddle_1 = Game_World.Paddle(190,30,'rect',100,5)
#    paddle_2 = Game_World.Paddle(190,590,'rect',100,5)
#    #piece together the initial frame
#    t_frame = Build_frame(board_1,ball_1, paddle_1,paddle_2)
#    return t_frame
    
def Build_frame(c_board,c_ball,c_paddle1,c_paddle2):
    #piece together the current frame
    #add the ball 

    t_board = np.zeros([c_board.height, c_board.width])
    t_board[:,:] = c_board.board
    t_board[c_ball.x-c_ball.r:c_ball.x+c_ball.r+1,c_ball.y-c_ball.r:c_ball.y+c_ball.r+1] = c_ball.ball
    t_board[c_paddle1.x:c_paddle1.x+c_paddle1.l,c_paddle1.y:c_paddle1.y+c_paddle1.w] = c_paddle1.paddle
    t_board[c_paddle2.x:c_paddle2.x+c_paddle2.l,c_paddle2.y:c_paddle2.y+c_paddle2.w] = c_paddle2.paddle    
    
    return t_board

######### MIAN LOOP ##################
#build initial board, game pieces, players and environment

#this was a function.... figure out how to make that work
#player1_name = input("Player 1 what is your name? ")
#player2_name = input("Player 2 what is your name? ")

#establish player info
player1 = Game_World.Player('o','l')
player2 = Game_World.Player('w','s')

#in lieu of an init function for start of game
board_1 = Game_World.GameBoard(480,620,20)
Game_World.GameBoard.Goal_build(board_1, 480)  
ball_1 = Game_World.Ball(240,320,0,0,10)
paddle_1 = Game_World.Paddle(190,30,'rect',100,5)
paddle_2 = Game_World.Paddle(190,590,'rect',100,5)


#piece together the initial frame
t_frame = Build_frame(board_1,ball_1, paddle_1,paddle_2)
#right way    
#frame = Game_Start()
plt.figure(1)
plt.imshow(t_frame, cmap = colm.get_cmap('Paired'),vmin = -5, vmax = 5)

#simulate motion
Game_World.Ball.Move_ball(ball_1,50,50)
Game_World.Paddle.Move_Paddle(paddle_1,50,0)
Game_World.Paddle.Move_Paddle(paddle_2,-50,0)

#buld frame #2
frame_2 = Build_frame(board_1,ball_1, paddle_1,paddle_2)

#build the GUI

plt.figure(2)
plt.imshow(frame_2, cmap = colm.get_cmap('Paired'),vmin = -5, vmax = 5)#plt.colorbar()
